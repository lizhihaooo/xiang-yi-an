# 湘易安医疗系统

#### 介绍
一个普通又不普通的医疗系统

#### 软件架构
软件架构说明 

![微信图片_20230217084151](C:\java_project\XiangYiAn\xiangyian-medical-parent\images\微信图片_20230217084151.jpg)

#### 安装教程

1.  需要配置环境

#### 配置修改

1. ​	修改application.yml配置文件:

   ```yml
   spring:
     # 连接池配置 使用先修改数据库名称 其余连接池配置不要轻易动
     datasource:
       url: jdbc:mysql://124.223.4.92:3306/xxx?useSSL=false&useUnicode=true&characterEncoding=UTF-8
       username: root
       password: 2878484868+1379462580xya  
       driver-class-name: com.mysql.jdbc.Driver
       type: com.alibaba.druid.pool.DruidDataSource
   #  若是需要用到缓存或是rabbitmq就打开 
   #  profiles:
   #    active: cache,mq
   mybatis-plus:
     mapper-locations: classpath:mappers/*.xml
     configuration:
       map-underscore-to-camel-case: true
       auto-mapping-behavior: full
       lazy-loading-enabled: true
       aggressive-lazy-loading: false
       #设置别名可以修改成自己的pojo 如果不需要用到xml就注释掉也是可以的
     type-aliases-package: com.xya.pojo
   
   ribbon:
     eager-load:
       enabled: true #开启饥饿加载提升第一次访问速度
       clients:
         - backstage-admin-category-service #开启自己的服务
   #微服务配置基本不动
   feign:
     httpclient:
       enabled: true  # 开启httpClient开关,启动连接池,提升feign连接效率!
       max-connections: 200  #最大连接数量
       max-connections-per-route: 50  #单路径最大连接数
   ```

2. bootstrap.yml配置基本不动 注册中心

   ```yml
   server:
     port: 3204 # 前端默认访问端口号为3000
     servlet:
       context-path: / # 前端默认访问的根路径
   spring:
     application:
       name: backstage-admin-category-service # 程序名就是服务名
     cloud:
       nacos:
         config:
           #  本机测试 如果注册中心不在本地 可以自行修改配置 
           #  连接服务器 
           server-addr: 124.223.4.92:8848 #注册中心
           group: DEFAULT_GROUP
           file-extension: yml
     config:
       import:
         - optional:nacos:${spring.application.name}.${spring.cloud.nacos.config.file-extension}
   ```

   

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

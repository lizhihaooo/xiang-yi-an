package com.xya.appointment.controller;

import com.xya.clients.TestClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/front/appointment")
public class TestController {
    @Resource
    private TestClients testClients;

    @RequestMapping("test")
    public Object test(){
        return  testClients.test();
    }

}

package com.xya.appointment;


import com.xya.clients.TestClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * projectName: front-desk-appointment
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
@EnableFeignClients(clients = {TestClients.class})
public class FrontDeskAppointmentApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontDeskAppointmentApplication.class, args);
    }
}
package com.xya.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.order.entity.Order;

import com.xya.utils.R;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-20
 */
public interface IOrderService extends IService<Order> {


    R list(Order order,int currentPage,int pageSize,String[] CreateTime);

    R add(Order order);

    R update(Order order);

    R remove(Long id);
}

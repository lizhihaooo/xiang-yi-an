package com.xya.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.order.mapper.OrderMapper;
import com.xya.order.entity.Order;
import com.xya.order.service.IOrderService;
import com.xya.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-20
 */
@Slf4j
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public R list(Order order,int currentPage,int pageSize,String[] CreateTime) {
        System.out.println(pageSize+"currentPage:"+currentPage);
        //参数封装
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        if(order.getUserId()!=null) {
            queryWrapper.eq("user_id",order.getUserId());
        }
        if(order.getOrderId()!=null){queryWrapper.eq("order_id",order.getOrderId());}
        if(order.getHospitalName()!=null){queryWrapper.eq("hospital_name",order.getHospitalName());}
        if(CreateTime!=null&&CreateTime.length>0){
            queryWrapper.ge("create_time",CreateTime[0]).le("create_time", CreateTime[1]);
        }
        IPage<Order> page = new Page<>(currentPage,pageSize);
        //数据库查询
        page = orderMapper.selectPage(page,queryWrapper);
        List<Order> recordList = page.getRecords();
        log.info("OrderServiceImpl.login业务结束，结果:{}","查询成功!");
        return R.ok("查询成功",recordList);
    }

    @Override
    public R add(Order order) {
        //1.插入数据
        int rows = orderMapper.insert(order);
        //2.插入成功
        if (rows == 0){
            log.info("OrderServiceImpl.login业务结束，结果:{}","添加失败!");
            return R.fail("插入订单失败!");
        }
        return R.ok("添加成功",orderMapper.selectById(order.getId()));
    }

    @Override
    public R update(Order order) {
        //1.更新数据
        int rows = orderMapper.updateById(order);
        //2.更新成功
        if (rows == 0){
            log.info("OrderServiceImpl.login业务结束，结果:{}","修改失败!");
            return R.fail("更新就诊人失败!");
        }

        return R.ok("修改成功",orderMapper.selectById(order.getId()));
    }

    @Override
    public R remove(Long id) {
        int rows = orderMapper.deleteById(id);

        if (rows == 0){
            log.info("InformationServiceImpl.remove业务结束，结果:{}","就诊人删除失败");
            return R.fail("删除就诊人数据失败!");
        }

        log.info("InformationServiceImpl.remove业务结束，结果:{}","就诊人删除成功!");

        return R.ok("就诊人删除成功!");
    }
}

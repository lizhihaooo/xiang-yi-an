package com.xya.order.param;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OrderRemoveParam {

    @NotBlank
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

}

package com.xya.order.controller;

import com.xya.order.entity.Order;
import com.xya.order.service.IOrderService;
import com.xya.order.param.OrderRemoveParam;
import com.xya.order.param.OrderListParam;
import com.xya.utils.R;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-20
 */
@RestController
@RequestMapping("/front/order")
public class OrderController {

    @Resource
    private IOrderService orderService;


    /**
     * 订单信息
     * @param orderListParam
     * @param result
     * @return
     */
    @PostMapping("/list")
    public R list(@RequestBody @Validated OrderListParam orderListParam, BindingResult result){
        return orderService.list(orderListParam.getOrder(),orderListParam.getCurrentPage(),orderListParam.getPageSize(),orderListParam.getCreateTime());
    }

    /**
     * 订单添加
     * @param order
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody @Validated Order order){

        return orderService.add(order);
    }

    /**
     * 订单修改
     * @param order
     * @return
     */
    @PostMapping("/update")
    public R updata(@RequestBody @Validated Order order){

        return orderService.update(order);
    }


    /**
     * 订单删除
     * @param orderIdParam
     * @return
     */
    @PostMapping("/remove")
    public R remove(@RequestBody @Validated OrderRemoveParam orderIdParam, BindingResult result){

        return orderService.remove(orderIdParam.getId());
    }


}

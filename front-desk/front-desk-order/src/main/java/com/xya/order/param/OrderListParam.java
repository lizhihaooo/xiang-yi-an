package com.xya.order.param;

import com.xya.order.entity.Order;
import lombok.Data;

@Data
public class OrderListParam {

    private Order order;
    private String[] CreateTime;
    private int currentPage=1;
    private int pageSize=5;
}

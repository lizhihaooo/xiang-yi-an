package com.xya.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.order.entity.Order;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-20
 */
public interface OrderMapper extends BaseMapper<Order> {

}

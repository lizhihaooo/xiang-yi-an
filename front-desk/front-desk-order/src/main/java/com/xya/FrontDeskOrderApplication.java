package com.xya;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: front-desk-order
 * @author: 李志豪
 * time: 2023/2/22  周三
 * description: 启动类
 */
@SpringBootApplication
@MapperScan(basePackages = "com.xya.order.mapper")
public class FrontDeskOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontDeskOrderApplication.class, args);
    }
}
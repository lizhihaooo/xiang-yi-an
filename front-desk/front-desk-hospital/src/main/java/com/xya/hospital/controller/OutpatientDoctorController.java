package com.xya.hospital.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@Controller
@RequestMapping("/outpatientDoctor")
public class OutpatientDoctorController {

}

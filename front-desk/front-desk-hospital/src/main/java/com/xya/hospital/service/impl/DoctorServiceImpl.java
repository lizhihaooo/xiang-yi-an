package com.xya.hospital.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.hospital.entity.Doctor;
import com.xya.hospital.entity.vo.DoctorVo;
import com.xya.hospital.mapper.DoctorMapper;
import com.xya.hospital.service.IDoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor> implements IDoctorService {
    @Resource
    private DoctorMapper doctorMapper;

    @Override
    public List<DoctorVo> doctorPositionList(String name) {
        String n="%";
        n+=name;
        n+="%";
        List<DoctorVo> doctorVos = doctorMapper.doctorPositionList(n);
        return doctorVos;
    }
}

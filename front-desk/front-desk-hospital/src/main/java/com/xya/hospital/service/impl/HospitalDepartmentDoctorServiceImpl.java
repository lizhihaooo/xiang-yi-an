package com.xya.hospital.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.hospital.entity.HospitalDepartmentDoctor;
import com.xya.hospital.mapper.HospitalDepartmentDoctorMapper;
import com.xya.hospital.service.IHospitalDepartmentDoctorService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@Service
public class HospitalDepartmentDoctorServiceImpl extends ServiceImpl<HospitalDepartmentDoctorMapper, HospitalDepartmentDoctor> implements IHospitalDepartmentDoctorService {

}

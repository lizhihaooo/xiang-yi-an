package com.xya.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.hospital.entity.HospitalDepartmentDoctor;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
public interface HospitalDepartmentDoctorMapper extends BaseMapper<HospitalDepartmentDoctor> {

}

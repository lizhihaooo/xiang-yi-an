package com.xya.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.hospital.entity.Area;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
public interface AreaMapper extends BaseMapper<Area> {

}

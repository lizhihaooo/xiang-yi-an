package com.xya.hospital.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xya.hospital.entity.Level;
import com.xya.hospital.service.ILevelService;
import com.xya.utils.R;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
@RestController
@RequestMapping("/level")
public class LevelController {
    @Resource
    private ILevelService iLevelService;

    @RequestMapping("/levelList")
    public R levelList(){
        return R.ok(iLevelService.list());
    }
}

package com.xya.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.hospital.entity.Mark;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
public interface MarkMapper extends BaseMapper<Mark> {

}

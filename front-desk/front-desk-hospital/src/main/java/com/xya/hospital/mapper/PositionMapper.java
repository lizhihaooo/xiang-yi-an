package com.xya.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.hospital.entity.Position;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
public interface PositionMapper extends BaseMapper<Position> {

}

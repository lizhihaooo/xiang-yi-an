package com.xya.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.hospital.entity.Doctor;
import com.xya.hospital.entity.vo.DoctorVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
public interface IDoctorService extends IService<Doctor> {
    List<DoctorVo> doctorPositionList(String name);
}

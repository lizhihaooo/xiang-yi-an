package com.xya.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.hospital.entity.Hospital;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
public interface HospitalMapper extends BaseMapper<Hospital> {

}

package com.xya.hospital.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
@Controller
@RequestMapping("/area")
public class AreaController {

}

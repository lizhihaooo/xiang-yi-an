package com.xya.hospital.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xya.hospital.entity.Area;
import com.xya.hospital.entity.Hospital;
import com.xya.hospital.entity.Level;
import com.xya.hospital.service.IAreaService;
import com.xya.hospital.service.IHospitalService;
import com.xya.hospital.service.ILevelService;
import com.xya.utils.R;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
@RestController
@RequestMapping("/hospital")
public class HospitalController {
    @Resource
    private IHospitalService hospitalService;
    @Resource
    private ILevelService levelService;
    @Resource
    private IAreaService areaService;

    private Map getLevelAndArea(){
        Map<String,Object> map=new HashMap();
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("deleted",0);
        List<Level> levelList = levelService.list(queryWrapper);
        System.out.println("levelList : " + levelList);
        List<Area> areaList = areaService.list(queryWrapper);
        System.out.println("areaList : " + areaList);
        map.put("levelList", levelList);
        map.put("areaList", areaList);
        return map;
    }
    //查询
    @RequestMapping("/list")
    public Map list(
            String name,
            @RequestParam(value = "levelId",required = false
                    ,defaultValue = "0") Long levelId,
            @RequestParam(value = "areaId",required = false
                    ,defaultValue = "0") Long areaId,
            @RequestParam(value = "hospitalState",required = false,
                    defaultValue = "1") Integer hospitalState) {
        //打印接收的值
        System.out.println("------------------------");
        System.out.println("levelId = "+levelId+", areaId = "+areaId+", hospitalState = "+hospitalState);
        //创建QueryWrapper对象，准备设置查询条件
        QueryWrapper<Hospital> queryWrapper=new QueryWrapper<>();
        queryWrapper.select("id","name","level_id")//只查询"id","name","level_id"三列的数据
                .eq("hospital_state",hospitalState) //查询状态为 1.正常使用 的医院
                .eq("deleted",0);//查询没有注销的医院 0/1
        //levelId不为0 ,则添加 levelId 条件
        if(levelId != 0L){
            queryWrapper.eq("level_id",levelId);
        }
        //areaId不为0 ,则添加 areaId 条件
        if(areaId != 0L){
            queryWrapper.eq("area_id",areaId);
        }
        //若name不为空 则根据name进行模糊查询
        if(!StringUtils.isEmpty(name)){
            queryWrapper.like("name",name);
        }
        List<Hospital> hospitalList = hospitalService.list(queryWrapper);
        //打印 hospitalList  确认数据
        System.out.println("hospitalList : " + hospitalList);
        Map map = getLevelAndArea();
        map.put("hospitalList", hospitalList);
        return map;
    }

    //根据id查询  查询医院详情
    @RequestMapping("/hospitalById")
    public R getHospitalById(@RequestParam(value = "id",required = false
            ,defaultValue = "0") Long id) {
        if(id == 0L){
            return R.fail("id已经失效");
        }
        Hospital byId = hospitalService.getById(id);
        return R.ok(byId);
    }

    //查询已经注销的医院
    @RequestMapping("/listByDeleted")
    public Object getListByDeleted() {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.select("id","name","level_id")//只查询"id","name","level_id"三列的数据
                .eq("deleted",1);//查询已经注销的所有医院
        List<Hospital> list = hospitalService.list(queryWrapper);
        System.out.println("list : " + list);
        return list;
    }

    //修改医院
    @PutMapping("/updateById")
    public boolean updState(@RequestBody Hospital hospital) {
        System.out.println("updState.hospital = " + hospital);
        boolean result = hospitalService.updateById(hospital);
        return result;
    }

    //医院逻辑删除
    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") Long id){
        System.out.println("delete.id = " + id);
        boolean result = hospitalService.removeById(id);
        return result;
    }

    //添加医院
    @RequestMapping("/add")
    public boolean addHospital(@RequestBody Hospital hospital){
        hospital.setHospitalState(2);//设置状态：2、未入驻 等审核员通过再修改状态
        System.out.println("addHospital.hospital = " + hospital);
        boolean save = hospitalService.save(hospital);
        return save;
    }

}

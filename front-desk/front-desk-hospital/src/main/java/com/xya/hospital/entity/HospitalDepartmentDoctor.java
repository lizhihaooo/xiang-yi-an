package com.xya.hospital.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@TableName("hospital_department_doctor")
@ApiModel(value = "HospitalDepartmentDoctor对象", description = "")
public class HospitalDepartmentDoctor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Long hospitalId;

    private Long departmentId;

    private Long doctorId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }
    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public String toString() {
        return "HospitalDepartmentDoctor{" +
            "id=" + id +
            ", hospitalId=" + hospitalId +
            ", departmentId=" + departmentId +
            ", doctorId=" + doctorId +
        "}";
    }
}

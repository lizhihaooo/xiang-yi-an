package com.xya.hospital.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@ApiModel(value = "Doctor对象", description = "")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("医生id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("医生姓名")
    private String name;

    @ApiModelProperty("医生职称id")
    private Integer positionId;

    @ApiModelProperty("医生状态(0上午班，1下午班，2全天班，3上半夜,4.下半夜,5休息)")
    private Integer doctorState;

    @ApiModelProperty("医生简介")
    private String briefIntroduction;

    @ApiModelProperty("医生头像")
    private String headTp;

    @ApiModelProperty("乐观锁(默认是0)")
    private Integer version;

    @ApiModelProperty("逻辑删除 (0未删除 1已删除)")
    @TableLogic
    private Integer deleted;

    @ApiModelProperty("创建时间(可以为null)")
    private LocalDateTime createTime;

    @ApiModelProperty("修改时间(可以为null)")
    private LocalDateTime updateTime;

    @ApiModelProperty("创建者(可以为null)")
    private Long creatorId;

    @ApiModelProperty("修改者(可以为null)")
    private Long updateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getPositionId() {
        return positionId;
    }

    public void setPositionId(Integer positionId) {
        this.positionId = positionId;
    }
    public Integer getDoctorState() {
        return doctorState;
    }

    public void setDoctorState(Integer doctorState) {
        this.doctorState = doctorState;
    }
    public String getBriefIntroduction() {
        return briefIntroduction;
    }

    public void setBriefIntroduction(String briefIntroduction) {
        this.briefIntroduction = briefIntroduction;
    }
    public String getHeadTp() {
        return headTp;
    }

    public void setHeadTp(String headTp) {
        this.headTp = headTp;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }
    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    @Override
    public String toString() {
        return "Doctor{" +
            "id=" + id +
            ", name=" + name +
            ", positionId=" + positionId +
            ", doctorState=" + doctorState +
            ", briefIntroduction=" + briefIntroduction +
            ", headTp=" + headTp +
            ", version=" + version +
            ", deleted=" + deleted +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", creatorId=" + creatorId +
            ", updateId=" + updateId +
        "}";
    }
}

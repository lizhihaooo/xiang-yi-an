package com.xya.hospital.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@ApiModel(value = "Mark对象", description = "")
public class Mark implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("放号id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("医生id")
    private Long doctorId;

    @ApiModelProperty("放号总数")
    private Integer markSum;

    @ApiModelProperty("已预约数")
    private Integer alreadyMakeCount;

    @ApiModelProperty("放号开始时间")
    private LocalDateTime markTime;

    @ApiModelProperty("放号结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty("乐观锁(默认是0)")
    private Integer version;

    @ApiModelProperty("逻辑删除 (0未删除 1已删除)")
    private Integer deleted;

    @ApiModelProperty("创建时间(可以为null)")
    private LocalDateTime createTime;

    @ApiModelProperty("修改时间(可以为null)")
    private LocalDateTime updateTime;

    @ApiModelProperty("创建者(可以为null)")
    private Long creatorId;

    @ApiModelProperty("修改者(可以为null)")
    private Long updateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }
    public Integer getMarkSum() {
        return markSum;
    }

    public void setMarkSum(Integer markSum) {
        this.markSum = markSum;
    }
    public Integer getAlreadyMakeCount() {
        return alreadyMakeCount;
    }

    public void setAlreadyMakeCount(Integer alreadyMakeCount) {
        this.alreadyMakeCount = alreadyMakeCount;
    }
    public LocalDateTime getMarkTime() {
        return markTime;
    }

    public void setMarkTime(LocalDateTime markTime) {
        this.markTime = markTime;
    }
    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }
    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    @Override
    public String toString() {
        return "Mark{" +
            "id=" + id +
            ", doctorId=" + doctorId +
            ", markSum=" + markSum +
            ", alreadyMakeCount=" + alreadyMakeCount +
            ", markTime=" + markTime +
            ", endTime=" + endTime +
            ", version=" + version +
            ", deleted=" + deleted +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", creatorId=" + creatorId +
            ", updateId=" + updateId +
        "}";
    }
}

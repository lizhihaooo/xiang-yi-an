package com.xya.hospital.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xya.hospital.entity.Doctor;
import com.xya.hospital.entity.vo.DoctorVo;
import com.xya.hospital.service.IDoctorService;
import com.xya.utils.R;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  医生控制层
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@RestController
@RequestMapping("/doctor")
@Api
@Slf4j
public class DoctorController {
    @Resource
    private IDoctorService doctorService;
    /*测试查询所有医生*/
    @GetMapping("/list")
    public R show(){
        List<Doctor> list = doctorService.list();
        System.out.println(list);
        return R.ok(list);
    }
    //条件模糊分页查询
    @GetMapping("/querypage")
    public R queryPage(String name,@RequestParam(value = "currentPage",defaultValue = "1") Integer currentPage,@RequestParam(value = "pageSize",defaultValue = "5") Integer pageSize){
        if (StringUtils.isEmpty(name)){//name为空
            log.info("DoctorController.queryPage：{}","name参数为空");
            List<Doctor> list = doctorService.list();
            //分页
            return R.ok("执行成功",list);
        }
        //PageHelper.startPage(currentPage,pageSize);

        //PageInfo pageInfo=new PageInfo(list);
        List<DoctorVo> doctorVos = doctorService.doctorPositionList(name);
        System.out.println("查到的值"+doctorVos);
        return R.ok("pageinfo");
    }
    //新增医生
    @PostMapping("/add")
    public R addDoctor(@RequestBody Doctor doctor){
        QueryWrapper queryWrapper=new QueryWrapper();
        //雪花算法生产id doctor.setId();
        boolean f = doctorService.save(doctor);
        if (!f){
            return R.fail("新增失败",f);
        }
        return R.ok("新增成功",f);
    }
    //修改医生
    @PutMapping("/update")
    public R updateDoctor(@RequestBody Doctor doctor){
        boolean f = doctorService.updateById(doctor);
        if (!f){
            return R.fail("修改失败",f);
        }
        return R.ok("修改成功",f);
    }
    //辞退医生
    @DeleteMapping("/delete/{id}")
    public R deleteDoctor(@PathVariable Long id){
        boolean f = doctorService.removeById(id);
        if (!f){
            return R.fail("删除失败",f);
        }
        return R.ok("删除成功",f);
    }

}

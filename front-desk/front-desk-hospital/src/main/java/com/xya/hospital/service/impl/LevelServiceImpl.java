package com.xya.hospital.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.hospital.entity.Level;
import com.xya.hospital.mapper.LevelMapper;
import com.xya.hospital.service.ILevelService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
@Service
public class LevelServiceImpl extends ServiceImpl<LevelMapper, Level> implements ILevelService {

}

package com.xya.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.hospital.entity.Doctor;
import com.xya.hospital.entity.vo.DoctorVo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
public interface DoctorMapper extends BaseMapper<Doctor> {

    public List<DoctorVo> doctorPositionList(String name);
}

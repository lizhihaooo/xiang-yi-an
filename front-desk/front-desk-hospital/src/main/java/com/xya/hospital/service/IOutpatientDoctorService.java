package com.xya.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.hospital.entity.OutpatientDoctor;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
public interface IOutpatientDoctorService extends IService<OutpatientDoctor> {

}

package com.xya.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.hospital.entity.Hospital;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
public interface IHospitalService extends IService<Hospital> {

}

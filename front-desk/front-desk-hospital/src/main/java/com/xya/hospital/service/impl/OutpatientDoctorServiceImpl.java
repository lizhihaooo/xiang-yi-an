package com.xya.hospital.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.hospital.entity.OutpatientDoctor;
import com.xya.hospital.mapper.OutpatientDoctorMapper;
import com.xya.hospital.service.IOutpatientDoctorService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@Service
public class OutpatientDoctorServiceImpl extends ServiceImpl<OutpatientDoctorMapper, OutpatientDoctor> implements IOutpatientDoctorService {

}

package com.xya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: front-desk-payment
 * @author: 李志豪
 * time: 2023/2/22  周三
 * description: 启动类
 */
@SpringBootApplication
public class FrontDeskPaymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontDeskPaymentApplication.class, args);
    }
}
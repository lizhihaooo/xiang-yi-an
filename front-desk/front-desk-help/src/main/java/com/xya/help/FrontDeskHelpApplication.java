package com.xya.help;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: front-desk-help
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
public class FrontDeskHelpApplication {
    public static void main(String[] args) {
        SpringApplication.run(FrontDeskHelpApplication.class, args);
    }
}
package com.xya.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.user.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
public interface UserMapper extends BaseMapper<User> {

    public List<User> userEcharts(@Param("date") String date);
}

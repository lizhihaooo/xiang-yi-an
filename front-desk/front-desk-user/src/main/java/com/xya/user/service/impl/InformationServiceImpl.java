package com.xya.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.user.mapper.InformationMapper;
import com.xya.user.entity.Information;
import com.xya.user.service.IInformationService;
import com.xya.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 就诊人信息表 服务实现类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@Slf4j
@Service
public class InformationServiceImpl extends ServiceImpl<InformationMapper, Information> implements IInformationService {

    @Resource
    private InformationMapper informationMapper;

    /**
     * 就诊人信息
     * @param information 账号参数 已经校验完毕
     * @return 检查结果 001  004
     */
    @Override
    public R list(Information information,int currentPage,int pageSize,String[] CreateTime) {
        //参数封装
        QueryWrapper<Information> queryWrapper = new QueryWrapper<>();
        if(information.getUserId()!=null) {
            queryWrapper.eq("user_id",information.getUserId());
        }
        if(information.getRealNameAuthentication()!=null) {
            queryWrapper.eq("real_name_authentication",information.getRealNameAuthentication());
        }
        if(information.getIdNumber()!=null) {
            queryWrapper.eq("id_number",information.getIdNumber());
        }
        IPage<Information> page = new Page<>(currentPage,pageSize);
        //数据库查询
        page = informationMapper.selectPage(page,queryWrapper);
        //数据库查询
        List<Information> informationList = page.getRecords();
        log.info("InformationServiceImpl.save业务结束，结果:{}","查询成功");
        return R.ok("查询成功",informationList);
    }



    @Override
    public R add(Information information) {

        //1.插入数据
        int rows = informationMapper.insert(information);
        //2.插入成功
        if (rows == 0){
            log.info("InformationServiceImpl.save业务结束，结果:{}","就诊人失败!");
            return R.fail("插入就诊人失败!");
        }
        return R.ok("添加成功",informationMapper.selectById(information.getId()));
    }

    @Override
    public R update(Information information) {
        //1.更新数据
        int rows = informationMapper.updateById(information);
        //2.更新成功
        if (rows == 0){
            log.info("InformationServiceImpl.save业务结束，结果:{}","就诊人失败!");
            return R.fail("更新就诊人失败!");
        }

        return R.ok("修改成功",informationMapper.selectById(information.getId()));
    }

    @Override
    public R remove(Long id) {
        int rows = informationMapper.deleteById(id);

        if (rows == 0){
            log.info("InformationServiceImpl.remove业务结束，结果:{}","就诊人删除失败");
            return R.fail("删除就诊人数据失败!");
        }

        log.info("InformationServiceImpl.remove业务结束，结果:{}","就诊人删除成功!");

        return R.ok("就诊人删除成功!");
    }

}

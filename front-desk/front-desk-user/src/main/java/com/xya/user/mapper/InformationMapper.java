package com.xya.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.user.entity.Information;

/**
 * <p>
 * 就诊人信息表 Mapper 接口
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
public interface InformationMapper extends BaseMapper<Information> {

}

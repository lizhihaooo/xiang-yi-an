package com.xya.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.user.param.UserCheckParam;
import com.xya.user.param.UserLoginParam;
import com.xya.user.entity.User;
import com.xya.utils.R;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
public interface IUserService extends IService<User> {

    /**
     * 检查账号信息
     * @param userCheckParam 账号参数 已经校验完毕
     * @return 检查结果 001  004
     */
    R check(UserCheckParam userCheckParam);

    /**
     * 登录业务
     * @param userLoginParam 账号和密码 已经校验 但是密码是明文!
     * @return 结果 001 004
     */
    R login(UserLoginParam userLoginParam);

    /**
     * 注册业务
     * @param user 参数已经校验,但是密码是明文!
     * @return 结果 001 004
     */
    R register(User user);

    /**
     * 密码修改业务
     * @param user
     * @return
     */
    R change_password(User user);


    /**
     *修改用户信息
     * @param user
     * @return
     */
    R update(User user);


    List<User> userEcharts(String date);
}

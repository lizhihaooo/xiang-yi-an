package com.xya.user.controller;


import com.xya.user.param.FeedbackListParam;
import com.xya.user.param.FeedbackRemoveParam;
import com.xya.user.entity.Feedback;
import com.xya.user.service.IFeedbackService;
import com.xya.utils.R;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * <p>
 * 意见反馈表 前端控制器
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@Controller
@RequestMapping("/front/feedback/")
public class FeedbackController {

    @Resource
    private IFeedbackService feedbackService;

    /**
     * 反馈信息
     * @param feedbackListParam
     * @param result
     * @return
     */
    @PostMapping("/list")
    public R list(@RequestBody @Validated FeedbackListParam feedbackListParam, BindingResult result){
        return feedbackService.list(feedbackListParam.getFeedback(),
                feedbackListParam.getCurrentPage(),
                feedbackListParam.getPageSize(),feedbackListParam.getCreateTime());
    }

    /**
     * 反馈添加
     * @param feedback
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody @Validated Feedback feedback){

        return feedbackService.add(feedback);
    }

    /**
     * 反馈修改
     * @param feedback
     * @return
     */
    @PostMapping("/update")
    public R updata(@RequestBody @Validated Feedback feedback){

        return feedbackService.update(feedback);
    }


    /**
     * 反馈修改
     * @param feedbackIdParam
     * @return
     */
    @PostMapping("/remove")
    public R remove(@RequestBody @Validated FeedbackRemoveParam feedbackIdParam, BindingResult result){

        return feedbackService.remove(feedbackIdParam.getId());
    }
}

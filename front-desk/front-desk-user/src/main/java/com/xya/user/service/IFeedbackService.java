package com.xya.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.user.entity.Feedback;
import com.xya.utils.R;

/**
 * <p>
 * 意见反馈表 服务类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
public interface IFeedbackService extends IService<Feedback> {

    R list(Feedback feedback,int currentPage,int pageSize,String[] CreateTime);

    R add(Feedback feedback);

    R update(Feedback feedback);

    R remove(Long id);

}

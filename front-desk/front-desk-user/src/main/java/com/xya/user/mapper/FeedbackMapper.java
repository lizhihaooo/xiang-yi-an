package com.xya.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.user.entity.Feedback;

/**
 * <p>
 * 意见反馈表 Mapper 接口
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

}

package com.xya.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.user.mapper.FeedbackMapper;
import com.xya.user.entity.Feedback;
import com.xya.user.service.IFeedbackService;
import com.xya.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 意见反馈表 服务实现类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@Slf4j
@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {

    @Resource
    private FeedbackMapper feedbackMapper;


    @Override
    public R list(Feedback feedback,int currentPage,int pageSize,String[] CreateTime) {
        //参数封装
        QueryWrapper<Feedback> queryWrapper = new QueryWrapper<>();
        if(feedback.getUserId()!=null) {
            queryWrapper.eq("user_id",feedback.getUserId());
        }
        if(feedback.getFeedbackType()!=null) {
            queryWrapper.eq("feedback_type",feedback.getFeedbackType());
        }
        if(CreateTime!=null&&CreateTime.length>0){
            queryWrapper.ge("create_time",CreateTime[0]).le("create_time", CreateTime[1]);
        }
        IPage<Feedback> page = new Page<>(currentPage,pageSize);
        //数据库查询
        page = feedbackMapper.selectPage(page,queryWrapper);
        //数据库查询
        List<Feedback> feedbacks = page.getRecords();
        log.info("FeedbackServiceImpl.login业务结束，结果:{}",feedbacks);
        return R.ok("查询成功",feedbacks);
    }

    @Override
    public R add(Feedback feedback) {
        //1.插入数据
        int rows = feedbackMapper.insert(feedback);
        //2.插入成功
        if (rows == 0){
            log.info("FeedbackServiceImpl.save业务结束，结果:{}","反馈失败!");
            return R.fail("插入反馈失败!");
        }
        return R.ok("添加成功",feedbackMapper.selectById(feedback.getId()));
    }

    @Override
    public R update(Feedback feedback) {
        //1.更新数据
        int rows = feedbackMapper.updateById(feedback);
        //2.更新成功
        if (rows == 0){
            log.info("FeedbackServiceImpl.save业务结束，结果:{}","反馈失败!");
            return R.fail("更新反馈失败!");
        }

        return R.ok("修改成功",feedbackMapper.selectById(feedback.getId()));
    }

    @Override
    public R remove(Long id) {
        int rows = feedbackMapper.deleteById(id);

        if (rows == 0){
            log.info("FeedbackServiceImpl.remove业务结束，结果:{}","反馈删除失败");
            return R.fail("删除反馈数据失败!");
        }

        log.info("FeedbackServiceImpl.remove业务结束，结果:{}","反馈删除成功!");

        return R.ok("反馈删除成功!");
    }
}

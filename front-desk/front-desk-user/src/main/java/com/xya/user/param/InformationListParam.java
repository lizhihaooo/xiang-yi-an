package com.xya.user.param;

import com.xya.user.entity.Information;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class InformationListParam {

    @NotBlank
    @JsonSerialize(using = ToStringSerializer.class)
    private Information information;
    private String[] CreateTime;
    private int currentPage=1;
    private int pageSize=5;
}

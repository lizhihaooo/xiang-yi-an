package com.xya.user.controller;

import com.xya.user.param.InformationListParam;
import com.xya.user.param.InformationRemoveParam;
import com.xya.user.entity.Information;
import com.xya.user.service.IInformationService;
import com.xya.utils.R;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 就诊人信息表 前端控制器
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@RestController
@RequestMapping("/front/information")
public class InformationController {

    @Resource
    private IInformationService iInformationService;

    /**
     * 就诊人信息
     * @param informationListParam
     * @param result
     * @return
     */
    @PostMapping("/list")
    public R list(@RequestBody @Validated InformationListParam informationListParam, BindingResult result){

        return iInformationService.list(informationListParam.getInformation(),
                informationListParam.getCurrentPage(),
                informationListParam.getPageSize(),
                informationListParam.getCreateTime());
    }

    /**
     * 就诊人添加
     * @param information
     * @return
     */
    @PostMapping("/save")
    public R save(@RequestBody @Validated Information information){

        return iInformationService.add(information);
    }

    /**
     * 就诊人修改
     * @param information
     * @return
     */
    @PostMapping("/update")
    public R updata(@RequestBody @Validated Information information){

        return iInformationService.update(information);
    }


    /**
     * 就诊人删除
     * @param information
     * @return
     */
    @PostMapping("/remove")
    public R remove(@RequestBody @Validated InformationRemoveParam information,BindingResult result){

        return iInformationService.remove(information.getId());
    }


}

package com.xya.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.user.entity.Information;
import com.xya.utils.R;


/**
 * <p>
 * 就诊人信息表 服务类
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
public interface IInformationService extends IService<Information> {

    /**
     * 就诊人信息
     * @param userId
     * @return
     */
    R list(Information information,int currentPage,int pageSize,String[] CreateTime);


    R add(Information information);

    R update(Information information);

    R remove(Long id);

}

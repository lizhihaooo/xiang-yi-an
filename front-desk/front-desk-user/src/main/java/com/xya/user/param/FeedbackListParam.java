package com.xya.user.param;

import com.xya.user.entity.Feedback;
import lombok.Data;

@Data
public class FeedbackListParam {

    private Feedback feedback;
    private String[] CreateTime;
    private int currentPage=1;
    private int pageSize=5;
}

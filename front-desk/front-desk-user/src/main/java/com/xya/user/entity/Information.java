package com.xya.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 就诊人信息表
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@TableName("visitor_information")
@ApiModel(value = "Information对象", description = "就诊人信息表")
public class Information implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("就诊人信息id")
    @TableId(value = "id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("用户 id  ")
    private Long userId;

    @ApiModelProperty("用户姓名")
    private String realNameAuthentication;

    @ApiModelProperty("证件类型")
    private Integer documentTypeId;

    @ApiModelProperty("证件号码")
    private String idNumber;

    @ApiModelProperty("性别(0女 1男)")
    private Integer gender;

    @ApiModelProperty("出生日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime birthday;

    @ApiModelProperty("手机号码")
    private String phoneNumber;

    @ApiModelProperty("婚姻状况(0已婚 1未婚)")
    private Integer maritalStatus;

    @ApiModelProperty("当前住址")
    private String currentAddress;

    @ApiModelProperty("详细地址")
    private String detailedAddress;

    @ApiModelProperty("是否专程来京就医(0是 1否)")
    private Integer medicalTreatment;

    @ApiModelProperty("创建者")
    private String createId;

    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @ApiModelProperty("修改者")
    private String updateId;

    @ApiModelProperty("逻辑删除")
    @TableLogic(value = "0",delval = "1")
    private Integer deleted;

    @ApiModelProperty("乐观锁")
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public String getRealNameAuthentication() {
        return realNameAuthentication;
    }

    public void setRealNameAuthentication(String realNameAuthentication) {
        this.realNameAuthentication = realNameAuthentication;
    }
    public Integer getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(Integer documentTypeId) {
        this.documentTypeId = documentTypeId;
    }
    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public Integer getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(Integer maritalStatus) {
        this.maritalStatus = maritalStatus;
    }
    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }
    public String getDetailedAddress() {
        return detailedAddress;
    }

    public void setDetailedAddress(String detailedAddress) {
        this.detailedAddress = detailedAddress;
    }
    public Integer getMedicalTreatment() {
        return medicalTreatment;
    }

    public void setMedicalTreatment(Integer medicalTreatment) {
        this.medicalTreatment = medicalTreatment;
    }
    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Information{" +
            "id=" + id +
            ", userId=" + userId +
            ", realNameAuthentication=" + realNameAuthentication +
            ", documentTypeId=" + documentTypeId +
            ", idNumber=" + idNumber +
            ", gender=" + gender +
            ", birthday=" + birthday +
            ", phoneNumber=" + phoneNumber +
            ", maritalStatus=" + maritalStatus +
            ", currentAddress=" + currentAddress +
            ", detailedAddress=" + detailedAddress +
            ", medicalTreatment=" + medicalTreatment +
            ", createId=" + createId +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", updateId=" + updateId +
            ", deleted=" + deleted +
            ", version=" + version +
        "}";
    }
}

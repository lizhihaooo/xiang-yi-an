package com.xya.user.controller;

import com.xya.user.param.UserCheckParam;
import com.xya.user.param.UserLoginParam;
import com.xya.user.entity.User;
import com.xya.user.service.IUserService;
import com.xya.utils.R;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@RestController
@RequestMapping("/front/desk/user")
public class UserController {

    @Resource
    private IUserService userService;

    /**
     * 注册
     * @param user
     * @param result
     * @return
     */
    @PostMapping("/register")
    public R register(@RequestBody @Validated User user, BindingResult result){

        if (result.hasErrors()){
            //如果存在异常,证明请求参数不符合注解要求
            return  R.fail("参数异常,不可注册!");
        }

        return userService.register(user);
    }

    /**
     * 登入
     * @param userLoginParam
     * @param result
     * @return
     */
    @PostMapping("/login")
    public R login(@RequestBody @Validated UserLoginParam userLoginParam, BindingResult result){
        System.out.println(userLoginParam);
        if (result.hasErrors()){
            //如果存在异常,证明请求参数不符合注解要求
            return  R.fail("参数异常,不可登录!");
        }

        return userService.login(userLoginParam);
    }

    /**
     * 修改密码
     * @param user
     * @return
     */
    @PostMapping("/password/rernewal")
    public R change_password(@RequestBody @Validated User user){
        return userService.change_password(user);
    }

    /**
     * 用户信息修改
     * @param user
     * @return
     */
    @PostMapping("/update")
    public R update(@RequestBody @Validated User user){
        return userService.update(user);
    }

    /**
     * 实名认证
     * @param user
     * @return
     */
    @PostMapping("/approve")
    public R Doctor(@RequestBody @Validated User user){
        return userService.update(user);
    }

    /**
     * 检查账号信息
     * @param userCheckParam 接收检查的账号实体 内部有参数校验注解
     * @param result 获取校验结果的实体对象
     * @return 返回封装结果R对象即可
     */
    @PostMapping("/check")
    public R check(@RequestBody @Validated UserCheckParam userCheckParam, BindingResult result){

        //检查是否符合检验注解的规则  符合 false  不符合 true
        boolean b = result.hasErrors();

        if (b){
            return R.fail("账号为null,不可使用!");
        }

        return userService.check(userCheckParam);
    }

    @GetMapping("/list")
    private Object list(){
        return userService.list();
    }


    //后端用户查询echarts图表所用
    @GetMapping("/user-echarts")
    public List<User> userEcharts(@RequestParam String date){
        System.out.println(date);
        return userService.userEcharts(date);
    }
}

package com.xya.controller;

import com.xya.entity.Department;
import com.xya.entity.vo.DepartmentOutpatientVo;
import com.xya.service.IDepartmentService;
import com.xya.utils.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
@RestController
@RequestMapping("/department")
@CrossOrigin
public class DepartmentController {

    @Resource
    private IDepartmentService departmentService;

    //单表查询
    @GetMapping("/dlist")
    public R dlist(){
        List<Department> dlist = departmentService.list();
        return  R.ok("查询成功",dlist);
    }
    //单表删除
    @DeleteMapping("/ddelete/{id}")
    public R ddelete(@RequestBody Long id){
        boolean b = departmentService.removeById(id);
        return R.ok("删除成功",b);
    }
    //单表添加
    @RequestMapping("/dinsert")
        public R dinsert(Department department){
        boolean save = departmentService.save(department);
        return  R.ok("添加成功",save);
    }

    //单表修改
    @PutMapping("/dupdate")
    public R dupdate(Department department){
        boolean b = departmentService.updateById(department);
        return R.ok("修改成功",b);
    }

    //预约挂号里面的科室和门诊
    @GetMapping("/departmentList/{id}")
    public R DepartmentList( @PathVariable("id") Long id){
       List<DepartmentOutpatientVo> list= departmentService.departmentOutList(id);
        return R.ok("查询成功",list);
    }



}

package com.xya.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xya.entity.Department;
import com.xya.entity.Outpatient;
import com.xya.service.IOutpatientService;
import com.xya.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
@RestController
@RequestMapping("/outpatient")
public class OutpatientController {
    @Autowired
    private IOutpatientService outpatientService;

    //停诊
    @RequestMapping("/list")
    public R lsit(   @RequestParam(required=false,defaultValue = "1") Integer pageNum,
                          @RequestParam(required = false,defaultValue = "6") Integer pageSize
                          ) {
        PageHelper.startPage(pageNum,pageSize);
        List<Outpatient> list = outpatientService.list();
        PageInfo<Outpatient> pageInfo=new PageInfo<>(list);
        Map map=new HashMap<>();
        map.put("pageInfo", pageInfo);
        return R.ok("查询成功",map) ;
    }

    //单表查询
    @GetMapping("/list")
    public R list(){
        List<Outpatient> list = outpatientService.list();
        return  R.ok("查询成功",list);
    }
    //单表删除
    @DeleteMapping("/delete/{id}")
    public R delete(@RequestBody Long id){
        boolean b = outpatientService.removeById(id);
        return R.ok("删除成功",b);
    }
    //单表添加
    @RequestMapping("/insert")
    public R insert(Outpatient outpatient){
        boolean save = outpatientService.save(outpatient);
        return  R.ok("添加成功",save);
    }

    //单表修改
    @PutMapping("/update")
    public R update(Outpatient outpatient){
        boolean b = outpatientService.updateById(outpatient);
        return R.ok("修改成功",b);
    }
}

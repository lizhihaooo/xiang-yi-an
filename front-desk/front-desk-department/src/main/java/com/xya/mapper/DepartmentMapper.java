package com.xya.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.entity.Department;
import com.xya.entity.vo.DepartmentOutpatientVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
public interface DepartmentMapper extends BaseMapper<Department> {

    List<DepartmentOutpatientVo> departmentOutList(Long id);
}

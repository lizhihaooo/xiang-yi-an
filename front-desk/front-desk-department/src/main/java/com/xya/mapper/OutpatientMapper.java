package com.xya.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.entity.Outpatient;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
public interface OutpatientMapper extends BaseMapper<Outpatient> {

}

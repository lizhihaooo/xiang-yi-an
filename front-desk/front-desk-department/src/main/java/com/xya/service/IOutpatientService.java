package com.xya.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.entity.Outpatient;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
public interface IOutpatientService extends IService<Outpatient> {


}

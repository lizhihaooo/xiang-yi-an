package com.xya.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.entity.Department;
import com.xya.entity.vo.DepartmentOutpatientVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
public interface IDepartmentService extends IService<Department> {

    List<DepartmentOutpatientVo> departmentOutList(Long id);
}

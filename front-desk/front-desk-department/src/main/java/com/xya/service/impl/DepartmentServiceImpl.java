package com.xya.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.entity.Department;
import com.xya.entity.vo.DepartmentOutpatientVo;
import com.xya.mapper.DepartmentMapper;
import com.xya.service.IDepartmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

    @Resource
    DepartmentMapper departmentMapper;

    @Override
    public List<DepartmentOutpatientVo> departmentOutList(Long id) {
        return departmentMapper.departmentOutList(id);
    }
}

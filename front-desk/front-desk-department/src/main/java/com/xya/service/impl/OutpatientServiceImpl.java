package com.xya.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.entity.Outpatient;
import com.xya.mapper.OutpatientMapper;
import com.xya.service.IOutpatientService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
@Service
public class OutpatientServiceImpl extends ServiceImpl<OutpatientMapper, Outpatient> implements IOutpatientService {


}

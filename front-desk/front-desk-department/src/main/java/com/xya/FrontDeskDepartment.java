package com.xya;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: front-desk-hospital
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
@MapperScan(basePackages = "com.xya.mapper")
public class FrontDeskDepartment {
    public static void main(String[] args) {
        SpringApplication.run(FrontDeskDepartment.class, args);
    }
}

package com.xya.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 陈波
 * @since 2023-02-21
 */
public class Outpatient implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 门诊id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 门诊名称
     */
    private String name;

    /**
     * 状态(1.正常使用,0.停诊)
     */
    private Integer outpatientState;

    /**
     * 门诊的描述
     */
    private String description;

    /**
     * 乐观锁(默认是0)
     */
    private Integer version;

    /**
     * 逻辑删除 (0未删除 1已删除)
     */
    private Integer deleted;

    /**
     * 创建时间(可以为null)
     */
    private LocalDateTime createTime;

    /**
     * 修改时间(可以为null)
     */
    private LocalDateTime updateTime;

    /**
     * 创建者(可以为null)
     */
    private Long creatorId;

    /**
     * 修改者(可以为null)
     */
    private Long updateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getOutpatientState() {
        return outpatientState;
    }

    public void setOutpatientState(Integer outpatientState) {
        this.outpatientState = outpatientState;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }
    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    @Override
    public String toString() {
        return "Outpatient{" +
            "id=" + id +
            ", name=" + name +
            ", outpatientState=" + outpatientState +
            ", description=" + description +
            ", version=" + version +
            ", deleted=" + deleted +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", creatorId=" + creatorId +
            ", updateId=" + updateId +
        "}";
    }
}

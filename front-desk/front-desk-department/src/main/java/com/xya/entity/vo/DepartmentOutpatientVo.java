package com.xya.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class DepartmentOutpatientVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 科室名称
     */
    private String departmentName;

    /**
     * 门诊名称
     */
    private String outpatientName;

}

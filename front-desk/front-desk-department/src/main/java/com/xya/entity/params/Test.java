package com.xya.entity.params;

import lombok.Data;

import java.io.Serializable;

@Data
public class Test implements Serializable {

    private String name;
    private Integer currentPage;
    private Integer pageSize;

}

package com.xya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * projectName: commons-gateway
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
public class CommonsGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommonsGatewayApplication.class, args);
    }
}

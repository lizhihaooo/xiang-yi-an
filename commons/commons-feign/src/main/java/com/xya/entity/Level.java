package com.xya.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
@TableName("hospital_level")
@Data
public class Level implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.ASSIGN_ID)//雪花算法
    private Long id;

    //等级名称
    private String name;

    //乐观锁(默认是0)
    @Version
    @TableField(select = false)
    private Integer version;

    //逻辑删除 (0未删除 1已删除)
    @TableLogic(value = "0",delval = "1")
    @TableField(select = false)
    private Integer deleted;

    //创建时间(可以为null)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT,select = false)
    private LocalDateTime createTime;

    //修改时间(可以为null)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE,select = false)
    private LocalDateTime updateTime;

    //创建者(可以为null)
    @TableField(select = false)
    private Long creatorId;

    //修改者(可以为null)
    @TableField(select = false)
    private Long updateId;
}

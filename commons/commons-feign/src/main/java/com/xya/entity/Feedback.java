package com.xya.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 意见反馈表
 * </p>
 *
 * @author zhouhui
 * @since 2023-02-16
 */
@ApiModel(value = "Feedback对象", description = "意见反馈表")
public class Feedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("意见反馈id")
    @TableId(value = "id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("意见反馈描述")
    private String feedbackDescribe;

    @ApiModelProperty("用户id")
    @JsonProperty("user_id")
    private Integer userId;

    @ApiModelProperty("反馈类型")
    @JsonProperty("feedback_type")
    private Integer feedbackType;

    @ApiModelProperty("创建时间")
    @JsonProperty("create_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    @ApiModelProperty("创建者")
    private String createId;

    @ApiModelProperty("修改者")
    private String updateId;

    @ApiModelProperty("逻辑删除")
    @TableLogic(value = "0",delval = "1")
    private Integer deleted;

    @ApiModelProperty("乐观锁")
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getFeedbackDescribe() {
        return feedbackDescribe;
    }

    public void setFeedbackDescribe(String feedbackDescribe) {
        this.feedbackDescribe = feedbackDescribe;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getFeedbackType() {
        return feedbackType;
    }

    public void setFeedbackType(Integer feedbackType) {
        this.feedbackType = feedbackType;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }
    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Feedback{" +
            "id=" + id +
            ", feedbackDescribe=" + feedbackDescribe +
            ", userId=" + userId +
            ", feedbackType=" + feedbackType +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", createId=" + createId +
            ", updateId=" + updateId +
            ", deleted=" + deleted +
            ", version=" + version +
        "}";
    }
}

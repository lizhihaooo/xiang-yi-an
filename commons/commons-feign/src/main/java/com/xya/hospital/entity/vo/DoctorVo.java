package com.xya.hospital.entity.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class DoctorVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("医生id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("医生姓名")
    private String name;

    @ApiModelProperty("医生职称id")
    private Integer positionId;

    @ApiModelProperty("医生状态(0上午班，1下午班，2全天班，3上半夜,4.下半夜,5休息)")
    private Integer doctorState;

    @ApiModelProperty("医生简介")
    private String briefIntroduction;

    @ApiModelProperty("医生头像")
    private String headTp;

    @ApiModelProperty("职称名称")
    private String position;

    @ApiModelProperty("医师服务费")
    private Double price;




}

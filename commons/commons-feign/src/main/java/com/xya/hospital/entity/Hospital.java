package com.xya.hospital.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author 曾志成
 * @since 2023-02-17
 */
@TableName("hospital")
@Data
public class Hospital implements Serializable {

    private static final long serialVersionUID = 1L;

    //医院id
    @TableId(value = "id",type = IdType.ASSIGN_ID)//雪花算法 ,type = IdType.ASSIGN_ID
    @JsonSerialize(using = ToStringSerializer.class)//防止前端获取的雪花id精度丢失
    private Long id;

    //医院名称
    private String name;

    //医院地址
    private String address;

    //医院介绍
    private String introduce;

    //医院头像
    private String images;

    //医院等级id
    private Long levelId;

    //医院地区id
    private Long areaId;

    //医院地区名称
    @TableField(exist = false)//别删
    private String areaName;

    //状态(1.正常使用，2.未入驻,3.停诊)
    private Integer hospitalState;

    //乐观锁(默认是0)
    @Version
    @TableField(select = false)
    private Integer version;

    //逻辑删除(0未删除、1已删除)
    @TableLogic(value = "0",delval = "1")
    @TableField(select = false)
    private Integer deleted;

    //创建时间(可以为null)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT,select = false)
    private LocalDateTime createTime;

    //修改时间(可以为null)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE,select = false)
    private LocalDateTime updateTime;

    //创建者(可以为null)
    @TableField(select = false)
    private Long creatorId;

    //修改者(可以为null)
    @TableField(select = false)
    private Long updateId;

}

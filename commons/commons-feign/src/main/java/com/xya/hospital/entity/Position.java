package com.xya.hospital.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lijiahao
 * @since 2023-02-21
 */
@TableName("doctor_position")
@ApiModel(value = "Position对象", description = "")
public class Position implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("医生职称id")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;


    @ApiModelProperty("职称名称")
    private String position;

    @ApiModelProperty("医师服务费")
    private Double price;

    @ApiModelProperty("乐观锁(默认是0)")
    private Integer version;

    @ApiModelProperty("逻辑删除 (0未删除 1已删除)")
    private Integer deleted;

    @ApiModelProperty("创建时间(可以为null)")
    private LocalDateTime createTime;

    @ApiModelProperty("修改时间(可以为null)")
    private LocalDateTime updateTime;

    @ApiModelProperty("创建者(可以为null)")
    private Long creatorId;

    @ApiModelProperty("修改者(可以为null)")
    private Long updateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }
    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    @Override
    public String toString() {
        return "Position{" +
            "id=" + id +
            ", position=" + position +
            ", price=" + price +
            ", version=" + version +
            ", deleted=" + deleted +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", creatorId=" + creatorId +
            ", updateId=" + updateId +
        "}";
    }
}

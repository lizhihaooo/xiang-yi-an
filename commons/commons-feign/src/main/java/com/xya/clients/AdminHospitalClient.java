package com.xya.clients;

import com.xya.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "front-desk-hospital-service")
public interface AdminHospitalClient {

    @PostMapping("/front/desk/hospital/findAll")
    R findAll(@RequestParam(value = "currentPage",required = false) Integer currentPage, @RequestParam(value = "pageSize",required = false) Integer pageSize);

}

package com.xya.clients;


import com.xya.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "front-desk-user-service")
public interface UserClient {

    //后端用户查询echarts图表所用
    @GetMapping("/front/desk/user/user-echarts")
    List<User> userEcharts(@RequestParam String date);
}

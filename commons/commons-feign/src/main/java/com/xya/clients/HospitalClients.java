package com.xya.clients;

import com.xya.entity.Level;
import com.xya.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(value = "front-desk-hospital-service")
public interface HospitalClients {

    //根据id查询  查询医院详情
    @RequestMapping("/hospital/hospitalById")
    R getHospitalById(@RequestParam(value = "id",required = false,defaultValue = "0") Long id);

    // 查询等级表
    @RequestMapping("/level/levelList")
    R levelList();
}

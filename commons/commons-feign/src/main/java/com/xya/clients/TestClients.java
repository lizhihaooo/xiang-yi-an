package com.xya.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "backstage-admin-category-service")

public interface TestClients {

    @RequestMapping("/backstage/category/list")
    Object test();

}

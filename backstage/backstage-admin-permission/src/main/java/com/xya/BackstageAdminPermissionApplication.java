package com.xya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: backstage-admin-permission
 *
 * @author: 李志豪
 * time: 2023/2/24  周五
 * description: 启动类
 */
@SpringBootApplication
public class BackstageAdminPermissionApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageAdminPermissionApplication.class, args);
    }
}

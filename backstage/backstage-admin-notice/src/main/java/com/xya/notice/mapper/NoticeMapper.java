package com.xya.notice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.notice.entity.Notice;

/**
 * <p>
 * 平台公告表 Mapper 接口
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-20
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}

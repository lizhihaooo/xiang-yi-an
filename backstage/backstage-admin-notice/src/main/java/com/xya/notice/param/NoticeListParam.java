package com.xya.notice.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NonNull;

import java.time.LocalDateTime;



@Data
public class NoticeListParam {

    private String createName;//创建者名称（医院名称/平台）

    private Integer noticeType;//公告类型（平台公告：0，医院专用公告：1,停诊专用公告：2)

    private String noticeName;//公告名称(标题）

    private Long creatorId;//公告创建者编号(平台：0，医院：医院编号)

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;//起始时间

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;//结束时间

    private Integer currentPage=1;//当前页数

    private Integer pageSize=5;//每页行数
}

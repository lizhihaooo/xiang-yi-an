package com.xya.notice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.notice.param.NoticeListParam;
import com.xya.notice.entity.Notice;
import com.xya.utils.R;

/**
 * <p>
 * 平台公告表 服务类
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-20
 */
public interface INoticeService extends IService<Notice> {

    R getAllNotice(NoticeListParam noticeListParam);

    R addNotice(Notice notice);

    R updateNoticeById(Notice notice);

    R delNoticeById(Long id);

    R selectNoticeById(Long id);
}

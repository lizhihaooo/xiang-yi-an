package com.xya.notice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 平台公告表
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-20
 */
@Data
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公告编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 公告类型（平台公告：0，医院专用公告：1，停诊专用公告：2)
     */
    private Long noticeType;

    /**
     * 创建者名称（医院名称/平台）
     */
    private String createName;

    /**
     * 公告名称(标题)
     */
    private String noticeName;

    /**
     * 公告内容(正文)
     */
    private String content;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 逻辑删除(默认0，删除：1)
     */
    @TableLogic
    private Integer deleted;

    /**
     * 公告创建者编号(平台：0，医院：医院编号)
     */
    private Long creatorId;

    /**
     * 公告发布日期(创建时间)
     */
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 修改时间(可以为null)
     */
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 修改者(可以为null)
     */
    private Long updateId;

}

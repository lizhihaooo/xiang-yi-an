package com.xya.notice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xya.notice.mapper.NoticeMapper;
import com.xya.notice.param.NoticeListParam;
import com.xya.notice.entity.Notice;
import com.xya.notice.service.INoticeService;
import com.xya.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 平台公告表 服务实现类
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-20
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

    @Resource
    private NoticeMapper noticeMapper;
    /**
     * 查询所有的方法
     * @param noticeListParam
     * @return
     */
    @Override
    public R getAllNotice(NoticeListParam noticeListParam) {
        PageHelper.startPage(noticeListParam.getCurrentPage(),noticeListParam.getPageSize());
        QueryWrapper queryWrapper=new QueryWrapper();
        //查询的判断条件

        //公告类型（平台公告：0，医院专用公告：1,停诊专用公告：2）【注：平台/用户查询所有公告时，可以不带参数。医院管理自己发送的公告时，必须带此条件】
        if(noticeListParam.getNoticeType()!=null){
            queryWrapper.eq("notice_type",noticeListParam.getNoticeType());
        }

        //创建者名称（医院名称/平台）,便于后台查询
        if(noticeListParam.getCreateName()!=null && noticeListParam.getCreateName().length()>0){
            queryWrapper.like("create_name",noticeListParam.getCreateName());
        }

        //根据公告标题查询
        if(noticeListParam.getNoticeName()!=null && noticeListParam.getNoticeName().length()>0){
            queryWrapper.like("notice_name",noticeListParam.getNoticeName());
        }

        //根据创建者编号查询（用于医院自己查询自家发出的公告）
        if(noticeListParam.getCreatorId()!=null){
            queryWrapper.eq("creator_id",noticeListParam.getCreatorId());
        }

        //时间查询，查询创建时间大于传入时间的数据
        if(noticeListParam.getStartTime()!=null){
            queryWrapper.gt( "create_time",noticeListParam.getStartTime());
        }

        //时间查询，查询创建时间小于传入时间的数据
        if(noticeListParam.getEndTime()!=null){
            queryWrapper.lt("create_time",noticeListParam.getEndTime());
        }

        List list = noticeMapper.selectList(queryWrapper);
        PageInfo pageInfo = new PageInfo<>(list);
        R r=R.ok("公告查询成功",pageInfo);
        return r;
    }

    /**
     * 发布公告的方法（添加方法）
     * @param notice 数据集合
     * @return
     */
    @Override
    public R addNotice(Notice notice) {
        //此处需要调用雪花算法给数据添加一个编号


        int insert = noticeMapper.insert(notice);
        if(insert>0){
            return R.ok("添加成功");
        }
        return R.fail("添加失败");
    }

    /**
     * 公告修改的方法
     * @param notice
     * @return
     */
    @Override
    public R updateNoticeById(Notice notice) {
        int i = noticeMapper.updateById(notice);
        if(i>0){
            return R.ok("修改成功");
        }
        return R.fail("修改失败");
    }

    /**
     * 撤回公告的方法
     * @param id 公告编号
     * @return
     */
    @Override
    public R delNoticeById(Long id) {
        int i = noticeMapper.deleteById(id);
        if(i>0){
            return R.ok("删除成功");
        }
        return R.fail("删除失败");
    }

    @Override
    public R selectNoticeById(Long id) {
        Notice notice = noticeMapper.selectById(id);
        return R.ok("查询成功",notice);
    }
}

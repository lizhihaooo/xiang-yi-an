package com.xya.notice.controller;

import com.xya.notice.param.NoticeListParam;
import com.xya.notice.entity.Notice;
import com.xya.notice.service.INoticeService;
import com.xya.utils.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 平台公告表 前端控制器
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-20
 */
@RestController
@RequestMapping("/backstage/admin/notice")
public class NoticeController {

    @Resource
    private INoticeService noticeService;

    /**
     * 公告查询（三端通用）
     * @param noticeListParam 查询时需要的参数
     * @return R
     */
    @PostMapping("/getAllNotice")
    public R getAllNotice(@RequestBody   NoticeListParam noticeListParam){
        R r=noticeService.getAllNotice(noticeListParam);
        return r;
    }

    /**
     * 发布公告的方法（医院/平台方使用）
     * @param notice 添加需要的参数
     * @return
     */
    @PostMapping("/addNotice")
    public R addNotice(@RequestBody Notice notice){
        R r=noticeService.addNotice(notice);
        return r;
    }

    /**
     * 修改公告的方法
     * @param notice
     * @return
     */
    @PostMapping("/updateNoticeById")
    public R updateNoticeById(@RequestBody Notice notice){
        R r=noticeService.updateNoticeById(notice);
        return r;
    }

    /**
     * 删除公告的方法
     * @param id
     * @return
     */
    @PostMapping("/delNoticeById/{id}")
    public R delNoticeById(@PathVariable(value = "id") String id){
        R r=noticeService.delNoticeById(Long.valueOf(id));
        return r;
    }

    /**
     * 指定查询的方法，根据公告编号查询公告详细信息（包括了公告内容）
     * @param id
     * @return
     */
    @PostMapping("/selectNoticeById/{id}")
    public R selectNoticeById(@PathVariable(value = "id") String id){
        R r=noticeService.selectNoticeById(Long.valueOf(id));
        return r;
    }

}

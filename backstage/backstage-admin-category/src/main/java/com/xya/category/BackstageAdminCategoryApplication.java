package com.xya.category;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: backstage-admin-category
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
public class BackstageAdminCategoryApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageAdminCategoryApplication.class, args);
    }
}
package com.xya.datareport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * projectName: backstage-admin-hospital
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
public class BackstageAdminDatareportApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageAdminDatareportApplication.class, args);
    }
}
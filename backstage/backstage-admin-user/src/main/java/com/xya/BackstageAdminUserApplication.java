package com.xya;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.xya.clients.UserClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * projectName: backstage-admin-user
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
@MapperScan(basePackages = "com.xya.user.mapper")
@EnableFeignClients(clients = {UserClient.class})
public class BackstageAdminUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageAdminUserApplication.class, args);
    }
}


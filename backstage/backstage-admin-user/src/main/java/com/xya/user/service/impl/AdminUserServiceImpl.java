package com.xya.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xya.user.entity.AdminUser;
import com.xya.user.mapper.AdminUserMapper;
import com.xya.user.params.LoginUserParam;
import com.xya.user.params.UserPage;
import com.xya.user.service.IAdminUserService;
import com.xya.utils.R;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lijian
 * @since 2023-02-17
 */
@Service
public class AdminUserServiceImpl extends ServiceImpl<AdminUserMapper, AdminUser> implements IAdminUserService {

    //登录
    @Override
    public AdminUser login(LoginUserParam loginUserParam) {
        //判断是否存在账号
        QueryWrapper<AdminUser> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("user_name",loginUserParam.getUserName());
        AdminUser user = baseMapper.selectOne(queryWrapper);
        return user;
    }

    @Override
    public R deleteById(Long id) {
        int i = baseMapper.deleteById(id);
        if(i<0){
            return R.fail("删除失败!!!",false);
        }
        return R.ok("删除成功",true);
    }

    @Override
    public R queryAll(UserPage page) {
        Page<AdminUser> page1=new Page<>(page.getCurrentPage(),page.getPageSize());
        Page<AdminUser> adminUserPage = baseMapper.selectPage(page1, null);

        return R.ok("查询成功",adminUserPage);
    }

}

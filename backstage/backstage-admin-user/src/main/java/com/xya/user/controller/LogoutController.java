package com.xya.user.controller;

import com.xya.utils.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping
public class LogoutController {

    @GetMapping("/logout")
    public R logout(HttpSession session){
        session.invalidate();
        return R.ok("注销成功",true);
    }
}

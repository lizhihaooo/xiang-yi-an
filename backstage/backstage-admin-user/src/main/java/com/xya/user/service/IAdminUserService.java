package com.xya.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.user.entity.AdminUser;
import com.xya.user.params.LoginUserParam;
import com.xya.user.params.UserPage;
import com.xya.utils.R;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lijian
 * @since 2023-02-17
 */
public interface IAdminUserService extends IService<AdminUser> {

    AdminUser login(LoginUserParam loginUserParam);

    R deleteById(Long id);

    R queryAll(UserPage page);
}

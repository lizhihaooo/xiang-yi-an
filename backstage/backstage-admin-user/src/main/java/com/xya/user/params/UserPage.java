package com.xya.user.params;


import lombok.Data;

@Data
public class UserPage {

    private Integer currentPage;

    private Integer pageSize;

}

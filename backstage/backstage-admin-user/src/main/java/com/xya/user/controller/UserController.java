package com.xya.user.controller;

import com.xya.user.service.UserService;
import com.xya.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api
public class UserController {

    @Resource
    private UserService userService;

    //图标数据展示的
    @GetMapping("/userEcharts")
    @ApiOperation("echarts图表")
    public R userCharts(String date){
        return userService.userCharts(date);
    }
}

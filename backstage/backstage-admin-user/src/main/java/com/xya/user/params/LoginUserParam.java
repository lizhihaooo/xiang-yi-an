package com.xya.user.params;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
public class LoginUserParam {


    private String userName;

    private String userPassword;

    private String code;

    private boolean remember;
}

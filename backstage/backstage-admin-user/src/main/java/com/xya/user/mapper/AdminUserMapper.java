package com.xya.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.user.entity.AdminUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lijian
 * @since 2023-02-17
 */
public interface AdminUserMapper extends BaseMapper<AdminUser> {

}

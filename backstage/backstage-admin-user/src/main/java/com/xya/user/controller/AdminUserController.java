package com.xya.user.controller;


import com.xya.user.entity.AdminUser;
import com.xya.user.params.LoginUserParam;
import com.xya.user.params.UserPage;
import com.xya.user.service.IAdminUserService;
import com.xya.utils.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lijian
 * @since 2023-02-17
 */
@RestController
@RequestMapping()
public class AdminUserController {

    @Resource
    private IAdminUserService userService;

    @PostMapping("/login")
    public R login(@RequestBody LoginUserParam loginUserParam, HttpServletRequest request, HttpSession session){
        System.out.println(loginUserParam);
        AdminUser user = userService.login(loginUserParam);

        if(null==user|| !user.getUserPassword().equals(loginUserParam.getUserPassword())){
            return R.fail("登录失败账号或密码错误",false);
        }
        String code = (String) request.getServletContext().getAttribute("code");

        if(!code.equalsIgnoreCase(loginUserParam.getCode())){
            return R.fail("验证码错误!!",false);
        }
        session.setAttribute("user",user);
        Map<String,Object> map=new HashMap<>();
        map.put("user",user);
        map.put("flag",true);
        return R.ok("登录成功!!",map);
    }

    @DeleteMapping("/deleteById/{id}")
    @ApiOperation("删除接口")
    public R deleteById(@PathVariable Long id){
           return userService.deleteById(id);
    }

    @PostMapping("queryAll")
    @ApiOperation("查询所有接口")
    public R queryAll(@RequestBody UserPage page){
        return userService.queryAll(page);
    }

 /*   @GetMapping("findById/{id}")
    @ApiOperation("根据id查询")
    public R findById(@PathVariable Long id){

    }*/


}

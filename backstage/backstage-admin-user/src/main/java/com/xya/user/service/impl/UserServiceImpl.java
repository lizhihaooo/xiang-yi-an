package com.xya.user.service.impl;

import com.xya.clients.UserClient;
import com.xya.entity.User;
import com.xya.user.service.UserService;
import com.xya.utils.R;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserClient userClient;

    @Override
    public R userCharts(String data) {

        List<User> users = userClient.userEcharts(data);

        Integer [] arr={0,0,0,0,0,0,0,0,0,0,0,0};

        for (User user:users){
            LocalDateTime createTime = user.getCreateTime();
            int monthValue = createTime.getMonthValue();

            if(monthValue==1){
                arr[0]+=1;
            }else if(monthValue==2){
                arr[1]+=1;
            }else if(monthValue==3){
                arr[2]+=1;
            }else if(monthValue==4){
                arr[3]+=1;
            }else if(monthValue==5){
                arr[4]+=1;
            }else if(monthValue==6){
                arr[5]+=1;
            }else if(monthValue==7){
                arr[6]+=1;
            }else if(monthValue==8){
                arr[7]+=1;
            }else if(monthValue==9){
                arr[8]+=1;
            }else if(monthValue==10){
                arr[9]+=1;
            }else if(monthValue==11){
                arr[10]+=1;
            }else{
                arr[11]+=1;
            }

        }

        return R.ok(arr);
    }
}

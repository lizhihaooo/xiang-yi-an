package com.xya.user.service;

import com.xya.utils.R;

public interface UserService {

    R userCharts(String date);
}

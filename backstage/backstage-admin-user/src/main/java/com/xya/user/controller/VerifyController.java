package com.xya.user.controller;

import com.xya.user.utils.VerificationCode;
import com.xya.utils.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

@RestController
@RequestMapping()
public class VerifyController {

    @GetMapping("/verifyCode")
        public void verifyCode(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        VerificationCode code = new VerificationCode();
        BufferedImage image = code.getImage();
        String text = code.getText();
        request.getServletContext().setAttribute("code",text.toString());
        VerificationCode.output(image,resp.getOutputStream());

    }
}

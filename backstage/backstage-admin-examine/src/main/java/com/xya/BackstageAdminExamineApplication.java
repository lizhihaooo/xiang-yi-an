package com.xya;

import com.xya.clients.HospitalClients;
import com.xya.clients.UserClient;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * projectName: backstage-admin-examine
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
@MapperScan(basePackages = "com.xya.examine.mapper")
@EnableFeignClients(clients = {HospitalClients.class})
public class BackstageAdminExamineApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageAdminExamineApplication.class, args);
    }
}
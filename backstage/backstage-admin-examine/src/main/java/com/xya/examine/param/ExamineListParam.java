package com.xya.examine.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;


/**
 * 查询审核表
 *  applicationType 审核申请类型（0：医院升级申请/1：医院入驻申请）
 *  auditStatus 审核状态(0:未审核，1：审核通过，2：驳回）
 *  startTime 起始时间
 *  endTime 结束时间
 *  currentPage 当前页数
 *  pageSize 每页行数
 */
@Data
public class ExamineListParam {


    //审核申请类型（0：医院升级申请/1：医院入驻申请）【添加时必须要填】
    private Integer applicationType;

    //审核状态
    private Integer auditStatus;

    //起始时间
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    //结束时间
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    //当前页数（默认：1）
    private Integer currentPage=1;

    //每页行数（默认：5）
    private Integer pageSize=5;

}

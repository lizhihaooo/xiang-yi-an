package com.xya.examine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 审核表
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-17
 */
public class Examine implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 审核编号(雪花算法)
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 医院id
     */
    private Long hospitalId;

    /**
     * 审核申请类型（0：医院升级申请/1：医院入驻申请）
     */
    private Integer applicationType;

    /**
     * 等级编号
     */
    private Long gradeId;

    /**
     * 审核材料(img)
     */
    private String reviewMaterials;

    /**
     * 审核状态(0:未审核，1：审核通过，2：驳回）
     */
    private Integer auditStatus;

    /**
     * 备注（驳回原因）
     */
    private String remarks;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 逻辑删除
     */
    //逻辑删除字段，标记当前记录是否被删除
    @TableLogic
    //@TableLogic(value = "0",delval = "1") 也可以在注解上设置删除字面值
    private Integer deleted;

    /**
     * 创建时间(可以为null)
     */
    @CreatedDate
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 修改时间(可以为null)
     */
    @LastModifiedDate
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 创建者(可以为null)
     */
    private Long creatorId;

    /**
     * 修改者(可以为null)
     */
    private Long updateId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }
    public Integer getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(Integer applicationType) {
        this.applicationType = applicationType;
    }
    public Long getGradeId() {
        return gradeId;
    }

    public void setGradeId(Long gradeId) {
        this.gradeId = gradeId;
    }
    public String getReviewMaterials() {
        return reviewMaterials;
    }

    public void setReviewMaterials(String reviewMaterials) {
        this.reviewMaterials = reviewMaterials;
    }
    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }
    public Long getUpdateId() {
        return updateId;
    }

    public void setUpdateId(Long updateId) {
        this.updateId = updateId;
    }

    @Override
    public String toString() {
        return "Examine{" +
            "id=" + id +
            ", hospitalId=" + hospitalId +
            ", applicationType=" + applicationType +
            ", gradeId=" + gradeId +
            ", reviewMaterials=" + reviewMaterials +
            ", auditStatus=" + auditStatus +
            ", remarks=" + remarks +
            ", version=" + version +
            ", deleted=" + deleted +
            ", createTime=" + createTime +
            ", updateTime=" + updateTime +
            ", creatorId=" + creatorId +
            ", updateId=" + updateId +
        "}";
    }
}

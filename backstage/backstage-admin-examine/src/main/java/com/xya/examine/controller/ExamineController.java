package com.xya.examine.controller;

import com.xya.examine.param.ExamineListParam;
import com.xya.examine.entity.Examine;
import com.xya.examine.service.IExamineService;
import com.xya.utils.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 审核表 前端控制器
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-17
 */
@RestController
@RequestMapping("/backstage/admin/examine")
public class ExamineController {

    @Resource
    private IExamineService iExamineService;

    @PostMapping("/list")
    public R getExamineList(
           @RequestBody ExamineListParam listParam
    ){
        R r = iExamineService.getExamineList(listParam);
        return r;
    }

    /**
     * 添加审核表的方法
     * @param examine 审核表实体类
     * @return r
     */
    @PostMapping("/addExamineById")
    public R addExamineById(@RequestBody Examine examine){
        R r = iExamineService.addExamineById(examine);
        return r;
    }


    /**
     * 修改审核表的方法
     * @param examine
     * @return
     */
    @PostMapping("/updateById")
    public R updateById(@RequestBody Examine examine){
        R r = iExamineService.updateExamineById(examine);
        return r;
    }

    /**
     * 指定查询（根据医院的编号查询审核信息，医院方查询所有使用）
     * @param hospitalId
     * @return
     */
    @PostMapping("/selectExamineByHospitalId/{hospitalId}")
    public R selectExamineByHospitalId(@PathVariable(value = "hospitalId") Long hospitalId){
        R r=iExamineService.selectExamineByHospitalId(hospitalId);
        return r;
    }


    /**
     * 指定查询，根据编号查询
     * @param id
     * @return
     */
    @PostMapping("/selectExamineById/{id}")
    public R selectExamineById(@PathVariable(value = "id") Long id){
        R r=iExamineService.selectExamineById(id);
        return r;
    }

    @PostMapping("/deleteExamineById/{id}")
    public R deleteExamineById(@PathVariable(value = "id") Long id){
        R r=iExamineService.deleteExamineById(id);
        return r;
    }
}

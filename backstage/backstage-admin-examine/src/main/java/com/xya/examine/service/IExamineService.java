package com.xya.examine.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xya.examine.param.ExamineListParam;
import com.xya.examine.entity.Examine;
import com.xya.utils.R;

/**
 * <p>
 * 审核表 服务类
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-17
 */
public interface IExamineService extends IService<Examine> {

    R getExamineList(ExamineListParam listParam);

    R addExamineById(Examine examine);

    R updateExamineById(Examine examine);

    R delById(Long id);

    R selectExamineByHospitalId(Long hospitalId);

    R selectExamineById(Long id);

    R deleteExamineById(Long id);
}

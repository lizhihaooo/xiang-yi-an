package com.xya.examine.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xya.clients.HospitalClients;
import com.xya.entity.Hospital;
import com.xya.entity.Level;
import com.xya.examine.mapper.ExamineMapper;
import com.xya.examine.param.ExamineListParam;
import com.xya.examine.entity.Examine;
import com.xya.examine.service.IExamineService;
import com.xya.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 * 审核表 服务实现类
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-17
 */
@Service
public class ExamineServiceImpl extends ServiceImpl<ExamineMapper, Examine> implements IExamineService {

    @Resource
    private HospitalClients hospitalClients;
    @Resource
    private ExamineMapper examineMapper;
    @Override
    public R getExamineList(ExamineListParam listParam) {

        PageHelper.startPage(listParam.getCurrentPage(),listParam.getPageSize());

        //判断传入的参数是否存在，存在则进行条件查询
        QueryWrapper queryWrapper=new QueryWrapper();
        //根据申请类型查询审核信息
        if(listParam.getApplicationType()!=null){
            queryWrapper.eq("application_type",listParam.getApplicationType());
        }
        //根据审核状态查询
        if(listParam.getAuditStatus()!=null){
            queryWrapper.eq("audit_status",listParam.getAuditStatus());
        }
        //查询创建时间大于起始时间的数据
        if(listParam.getStartTime()!=null){
            queryWrapper.gt("create_time",listParam.getStartTime());
        }
        //查询创建时间小于结束时间的数据
        if(listParam.getEndTime()!=null){
            queryWrapper.lt("create_time",listParam.getEndTime());
        }

        List list = examineMapper.selectList(queryWrapper);
        PageInfo<Examine> pageInfo = new PageInfo<Examine>(list);

        //循环遍历出医院的编号，用set存，去重复，减少后续重复查询医院的次数
        Set<Long> set=new HashSet();
        for(Examine o:pageInfo.getList()){
            set.add(o.getHospitalId());
        }

        //查询出审核表后，根据医院编号查询所有的医院
        ArrayList hospitalIdList=new ArrayList();
        for(Long o:set){
            Object data = hospitalClients.getHospitalById(o).getData();
            hospitalIdList.add(data);
        }

        //查询等级表信息
        Object levelData = hospitalClients.levelList().getData();

        //将返回的医院数据与查询到的审核表数据一起发送到前端
        Map<String,Object> map=new HashMap<>();
        map.put("pageInfo",pageInfo);
        map.put("levelList",levelData);
        map.put("hospitalIdList",hospitalIdList);

        return R.ok("审核查询",map);
    }

    /**
     * 添加审核表信息
     * 医院发起 入驻/信息改变 请求
     * @param examine 审核表信息
     * @return
     */
    @Override
    public R addExamineById(Examine examine) {
        R r;
        int insert = examineMapper.insert(examine);
        if(insert>0){
            r=R.ok("审核申请添加成功");
        }else{
            r=R.fail("审核申请添加失败");
        }
        return r;
    }

    /**
     * 修改审核表的方法
     * @param examine 审核表信息
     *       只修改状态，且完成修改后修改对应医院的状态
     * @return
     */
    @Override
    public R updateExamineById(Examine examine) {
        //修改审核表状态（当状态为驳回时，需要添加备注【前台判断】）
        int i = examineMapper.updateById(examine);
        //修改完成后修改医院的状态/信息（可能需要修改医院的等级编号）



        return  R.ok("审核完成");
    }

    /**
     * 逻辑删除
     * @param id
     * @return
     */
    @Override
    public R delById(Long id) {
        int rows = examineMapper.deleteById(id);
        if (rows > 0){
            return R.ok("删除用户数据成功!");
        }
        return R.fail("删除用户数据失败!");
    }

    /**
     * 查询审核表（根据医院编号查询)
     * @param hospitalId 医院编号
     * @return
     */
    @Override
    public R selectExamineByHospitalId(Long hospitalId) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("hospital_id",hospitalId);
        List list = examineMapper.selectList(queryWrapper);
        return R.ok("查询成功",list);
    }

    /**
     * 指定查询，根据编号查询
     * @param id
     * @return
     */
    @Override
    public R selectExamineById(Long id) {
        //查询审核表信息
        Examine examine = examineMapper.selectById(id);
        //查询医院表信息
        Object hospital = hospitalClients.getHospitalById(examine.getHospitalId()).getData();
        Map map=new HashMap();
        map.put("examine",examine);
        map.put("hospital",hospital);
        return R.ok("查询成功",map);
    }

    /**
     * 删除的方法
     * @param id
     * @return
     */
    @Override
    public R deleteExamineById(Long id) {
        int i = examineMapper.deleteById(id);
        if(i>0){
            return R.ok("删除成功");
        }else{
            return R.fail("删除失败");
        }
    }
}

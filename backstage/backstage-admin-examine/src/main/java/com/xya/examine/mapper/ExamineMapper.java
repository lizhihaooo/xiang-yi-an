package com.xya.examine.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xya.examine.entity.Examine;

/**
 * <p>
 * 审核表 Mapper 接口
 * </p>
 *
 * @author 蒋炜
 * @since 2023-02-17
 */
public interface ExamineMapper extends BaseMapper<Examine> {

}

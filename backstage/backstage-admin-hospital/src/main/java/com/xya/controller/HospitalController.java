package com.xya.controller;


import com.xya.service.HospitalService;
import com.xya.utils.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 李志豪
 */
@RestController
public class HospitalController {

    @Resource
    private HospitalService hospitalService;

    @PostMapping("/findAll")
    public R findAll(@RequestParam(required = false,defaultValue = "1") Integer currentPage,@RequestParam(required = false,defaultValue = "5") Integer pageSize){
        return hospitalService.findAll(currentPage,pageSize);
    }
}

package com.xya.service.impl;

import com.xya.clients.AdminHospitalClient;
import com.xya.service.HospitalService;
import com.xya.utils.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class HospitalServiceImpl implements HospitalService {

    @Resource
    private AdminHospitalClient adminHospitalClient;
    @Override
    public R findAll(Integer currentPage, Integer pageSize) {
        R r = adminHospitalClient.findAll(currentPage, pageSize);
        return r;
    }
}

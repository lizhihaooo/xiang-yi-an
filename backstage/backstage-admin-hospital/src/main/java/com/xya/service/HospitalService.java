package com.xya.service;

import com.xya.utils.R;
import org.springframework.stereotype.Service;

public interface HospitalService {
    R findAll(Integer currentPage, Integer pageSize);
}

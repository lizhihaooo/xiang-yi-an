package com.xya;

import com.xya.clients.AdminHospitalClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * projectName: backstage-admin-hospital
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
@EnableFeignClients(clients = {AdminHospitalClient.class})
public class BackstageAdminHospitalApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackstageAdminHospitalApplication.class, args);
    }
}

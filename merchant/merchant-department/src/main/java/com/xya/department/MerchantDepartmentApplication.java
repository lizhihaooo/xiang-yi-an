package com.xya.department;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * projectName: merchant-department
 *
 * @author: 李志豪
 * time: 2023/2/14  周二
 * description: 启动类
 */
@SpringBootApplication
//开启feign的客户端
@EnableFeignClients()
public class MerchantDepartmentApplication {
    public static void main(String[] args) {
        SpringApplication.run(MerchantDepartmentApplication.class, args);
    }
}
